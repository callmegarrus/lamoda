from functools import wraps
from flask import session

from utils import json_answer
from models import User

def requires_login(need_admin=False):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):  
            if 'user' not in session:
                return json_answer('Please login!', 403)

            user = User.query.filter_by(login=session['user']).first()
            
            if need_admin and not user.is_admin:
                return json_answer('Please login as admin!', 403)

            return f(user, *args, **kwargs)
        return wrapped
    return wrapper