# -*- coding: utf-8 -*-
from flask import jsonify

def json_answer(description, code=200):
    answer = {'message': description}
    if code != 200:
        answer['status'] = 'error'
    else:
        answer['status'] = 'ok'
    return jsonify(answer), code

def add_to_db_by_session(session, obj):
    session.add(obj)
    session.commit()