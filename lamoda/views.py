# -*- coding: utf-8 -*-
from flask import Flask, request, session

from utils import json_answer, add_to_db_by_session
from models import User, db, bcrypt
from permissions import requires_login

app = Flask(__name__)


@app.route('/api/register', methods=['POST'])
def register():
    post_data = request.get_json(force=True)
    user = User.query.filter_by(login=post_data.get('login')).first()
    
    if not user:
        try:
            # при создании можно указать что пользователь является админом
            # делаю это в целях облегчения демонстрации разграничения прав
            user = User(
                login=post_data.get('login'),
                password=post_data.get('password'),
                is_admin=post_data.get('is_admin')
            )

            add_to_db_by_session(db.session, user)

            return json_answer('Successfully registered.')
        except:
            return json_answer('Some error occurred. Please try again.', 401)
    else:
        return json_answer('User already exists. Please Log in.', 202)

@app.route('/api/login', methods=['POST'])
def login():
    post_data = request.get_json(force=True)
    user = User.query.filter_by(login=post_data['login']).first()
    
    if user and bcrypt.check_password_hash(user.password, post_data['password']):
        session['user'] = user.login
        
        return json_answer('Successful login.')
    else:
        return json_answer('Unsuccessful login.', 202)

@app.route('/api/logout', methods=['POST'])
def logout():
    session.pop('user', None)
    return json_answer('Successful logout.')


@app.route('/', methods=['GET', 'POST'])
@requires_login(need_admin=False)
def home(user):
    return json_answer('Hello, {}!'.format(user.login))

@app.route('/admin', methods=['GET', 'POST'])
@requires_login(need_admin=True)
def admin(user):
    return json_answer('Hello, {}! Welcome to admin area!'.format(user.login))

if __name__ == '__main__':
    app.debug = True
    app.secret_key = "secret"
    app.run()
